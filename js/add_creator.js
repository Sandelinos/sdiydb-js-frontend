import { creatorLinkTypes } from '/js/modules/constants.js';
import {
	backendRequest,
	drawJson,
	formatURL,
	newRemoveButton
} from '/js/modules/utils.js';

// Insert a link
const addLink = (first) => {
	// Type
	const typeLabelSpan = document.createElement('span');
	typeLabelSpan.innerHTML = 'Type';

	const typeDropdown = document.createElement('select');
	typeDropdown.className = 'linkType';

	for (let type in creatorLinkTypes) {
		const typeOption = document.createElement('option');
		typeOption.value = type;
		typeOption.innerHTML = creatorLinkTypes[type];
		typeDropdown.appendChild(typeOption);
	}

	const typeLabel = document.createElement('label');
	typeLabel.append(
		typeLabelSpan,
		typeDropdown
	);

	// URL
	const urlLabelSpan = document.createElement('span');
	urlLabelSpan.innerHTML = 'URL';

	const urlField = document.createElement('input');
	urlField.className = 'linkUrl';
	urlField.type = 'url';
	urlField.placeholder = 'URL';

	const urlLabel = document.createElement('label');
	urlLabel.append(
		urlLabelSpan,
		urlField
	);

	// Link fields div
	const linkContainerDiv = document.createElement('div');
	linkContainerDiv.append(
		typeLabel,
		urlLabel
	);

	// Link div
	const linkContainer = document.createElement('div');
	linkContainer.className = 'link';
	linkContainer.appendChild(linkContainerDiv);

	// Link button
	if (first) {
		// Add button
		const addLinkButton = document.createElement('button');
		addLinkButton.type = 'button';
		addLinkButton.innerHTML = '+';
		addLinkButton.addEventListener('click', () => addLink(false));
		linkContainer.appendChild(addLinkButton);
	} else {
		// Removal button
		linkContainer.id = 'link' + linkID;
		linkContainer.appendChild(newRemoveButton('link' + linkID));
		linkID += 1;
	}

	document.getElementById('links').appendChild(linkContainer);
};

// Create new creatorObject from fields
const newCreatorObject = () => {
	const creatorObject = {};

	// Name & brand
	creatorObject.name =
		document.getElementById('creatorName').value || undefined;
	creatorObject.brand =
		document.getElementById('creatorBrand').value || undefined;

	// Links
	creatorObject.links = [];
	const links = document.getElementsByClassName('link');
	for (let link of links) {
		const linkObject = {};
		linkObject.type = link.getElementsByClassName('linkType')[0].value;
		linkObject.url = link.getElementsByClassName('linkUrl')[0].value;

		creatorObject.links.push(linkObject);
	}

	return creatorObject;
};

// Submit creator to backend
const submitCreator = () => {
	backendRequest(
		'/creator',
		(data) => {
			window.location.href = formatURL('/creator', { id: data.id });
		},
		newCreatorObject()
	);
};

let linkID = 0;

addLink(true);

// Attach listeners to Submit & Preview buttons
document.getElementById('previewButton')
	.addEventListener('click', () => drawJson(newCreatorObject()));
document.getElementById('submitButton')
	.addEventListener('click', submitCreator);
