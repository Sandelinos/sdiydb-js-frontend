import { backendRequest, formatURL } from '/js/modules/utils.js';

const register = () => {
	const user = document.getElementById('userField').value;
	const password = document.getElementById('passwordField').value;
	const password2 = document.getElementById('passwordField2').value;
	const token = document.getElementById('tokenField').value;

	if (password !== password2) {
		alert('Passwords don\'t match!');
		return;
	}

	backendRequest(
		'/register',
		() => {
			alert('Registration succesful!');
			window.location.href = formatURL('/login');
		},
		{
			user: user,
			password: password,
			registration_token: token
		}
	);
};

document.getElementById('registerButton').addEventListener('click', register);
