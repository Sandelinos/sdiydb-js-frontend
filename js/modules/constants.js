export const creatorLinkTypes = {
	'website': 'Website 🌐',
	'git': 'Git ⚙️',
	'store': 'Store 🛒',
	'video': 'Videos ▶️',
	'support': 'Support 💌',
};

export const resourceTypes = {
	'schematic': 'Schematic',
	'gerber': 'Gerber',
	'pcb-etching-print': 'PCB etching print',
	'panel-template': 'Panel template',
	'code': 'Code',
	'stripboard-layout': 'Stripboard layout',
	'protoboard-layout': 'Protoboard layout',
	'build-video': 'Build video',
};

export const formats = {
	'eurorack': 'Eurorack',
	'kosmo': 'Kosmo',
};
