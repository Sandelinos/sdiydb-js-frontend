export const urlParams = new URLSearchParams(window.location.search);

export const drawJson = (data) => {
	document.getElementById('jsonData').innerHTML = JSON.stringify(data, null, '\t');
};

export const formatURL = (path, params = null) => {
	let url = path;
	#ifndef pretty_urls
	url += '.html';
	#endif
	if (params) {
		const searchParams = new URLSearchParams(params);
		url += '?' + searchParams.toString();
	}
	return url;
};

export const backendRequest = (path, handler, body = null) => {
	const fetchPromise = fetch(
		'http://127.0.0.1:3000' + path,
		{
			method: body ? 'POST' : 'GET',
			body: body ? JSON.stringify(body) : undefined,
			headers: {
				'Content-type': 'application/json'
			},
			credentials: body ? 'include' : undefined,
		}
	);

	fetchPromise.then(response => {
		if (response.ok) {
			if (response.headers.get('content-type') == 'application/json') {
				response.json().then(handler);
			} else {
				handler();
			}
		} else {
			if (response.headers.get('content-type') == 'application/json') {
				response.json().then(data => {
					alert('Got error from backend:\n' + data.errorMessage);
				});
			} else {
				response.text().then(text => {
					alert('Unknown error from backend:\n' + text);
				});
			}
		}
	});

	fetchPromise.catch(error => {
		alert('Failed to connect to backend:\n' + error.message);
	});
};

export const formatCreatorName = (creator) => {
	if (creator.name && creator.brand) {
		return creator.name + ' (' + creator.brand + ')';
	} else {
		return creator.name || creator.brand;
	}
};

const removeElement = (event) => {
	document.getElementById(event.currentTarget.target).remove();
};

export const newRemoveButton = (target) => {
	const removeButton = document.createElement('button');
	removeButton.type = 'button';
	removeButton.innerHTML = '-';
	removeButton.target = target;
	removeButton.addEventListener('click', removeElement);

	return removeButton;
};
