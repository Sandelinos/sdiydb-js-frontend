import { backendRequest, formatCreatorName, formatURL } from '/js/modules/utils.js';

const drawCreators = (creators) => {
	const creatorListNode = document.getElementById('creators');
	creators.forEach(creator => {
		const creatorNode = document.createElement('li');
		const linkNode = document.createElement('a');
		linkNode.innerHTML = formatCreatorName(creator);
		linkNode.href = formatURL('/creator', { id: creator.id });
		creatorNode.appendChild(linkNode);
		creatorListNode.appendChild(creatorNode);
	});
};

backendRequest('/list_creators', drawCreators);
