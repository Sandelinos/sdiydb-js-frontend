import { backendRequest, drawJson, formatCreatorName, formatURL, urlParams } from '/js/modules/utils.js';
import { creatorLinkTypes } from '/js/modules/constants.js';

const drawCreator = (creator) => {
	drawJson(creator);

	// Name
	document.getElementById('creatorName').innerHTML = formatCreatorName(creator);

	// Links
	creator.links.forEach(link => {
		const linkNode = document.createElement('li');
		const hyperlinkNode = document.createElement('a');
		hyperlinkNode.innerHTML = creatorLinkTypes[link.type];
		hyperlinkNode.href = link.url;
		linkNode.appendChild(hyperlinkNode);
		document.getElementById('creatorLinks').appendChild(linkNode);
	});
};

const drawModules = (modules) => {
	const moduleListNode = document.getElementById('creatorModules');
	modules.forEach(module => {
		const moduleNode = document.createElement('li');
		const linkNode = document.createElement('a');
		linkNode.innerHTML = module.name;
		linkNode.href = formatURL('/module', { id: module.id });
		moduleNode.appendChild(linkNode);
		moduleListNode.appendChild(moduleNode);
	});
};

backendRequest('/creator/' + urlParams.get('id'), drawCreator);
backendRequest('/list_modules?creator=' + urlParams.get('id'), drawModules);
