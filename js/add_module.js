import { formats, resourceTypes } from '/js/modules/constants.js';
import {
	backendRequest,
	drawJson,
	formatURL,
	newRemoveButton
} from '/js/modules/utils.js';

const newPlusButton = (onClickFunction) => {
	const plusButton = document.createElement('button');
	plusButton.type = 'button';
	plusButton.innerHTML = '+';
	plusButton.addEventListener('click', onClickFunction);
	return plusButton;
};

const newDropdownOption = (value, text) => {
	const dropdownOption = document.createElement('option');
	dropdownOption.value = value;
	dropdownOption.innerHTML = text;
	return dropdownOption;
};

const newCreatorInput = (creators) => {
	const creatorLabel = document.createElement('label');
	const creatorLabelSpan = document.createElement('span');
	creatorLabelSpan.innerHTML = 'Creator';
	creatorLabel.appendChild(creatorLabelSpan);

	const creatorDropdown = document.createElement('select');
	creatorDropdown.className = 'moduleCreator';
	creatorDropdown.appendChild(newDropdownOption('', '-'));

	creators.forEach(creator => {
		creatorDropdown.appendChild(
			newDropdownOption(creator.id, creator.brand || creator.name)
		);
	});

	creatorLabel.appendChild(creatorDropdown);
	return creatorLabel;
};

const newParentInput = (modules) => {
	const parentLabel = document.createElement('label');
	const parentLabelSpan = document.createElement('span');
	parentLabelSpan.innerHTML = 'Based on';
	parentLabel.appendChild(parentLabelSpan);

	const parentDropdown = document.createElement('select');
	parentDropdown.className = 'moduleParent';
	parentDropdown.appendChild(newDropdownOption('', '-'));

	modules.forEach(module => {
		parentDropdown.appendChild(
			newDropdownOption(module.id, module.name)
		);
	});

	parentLabel.appendChild(parentDropdown);
	return parentLabel;
};

const addCreator = (creators) => {
	const creatorContainer = document.createElement('div');
	creatorContainer.id = 'creator' + creatorID;
	creatorContainer.append(
		newCreatorInput(creators),
		newRemoveButton('creator' + creatorID)
	);

	creatorID += 1;
	document.getElementById('creators').appendChild(creatorContainer);
};

const addParent = (modules) => {
	const parentContainer = document.createElement('div');
	parentContainer.id = 'parent' + parentID;
	parentContainer.append(
		newParentInput(modules),
		newRemoveButton('parent' + parentID)
	);

	parentID += 1;
	document.getElementById('parents').appendChild(parentContainer);
};

const addFirstCreator = (creators) => {
	const firstCreatorContainer = document.createElement('div');
	firstCreatorContainer.append(
		newCreatorInput(creators),
		newPlusButton(() => addCreator(creators))
	);
	document.getElementById('creators').appendChild(firstCreatorContainer);
};

const addFirstParent = (modules) => {
	const firstParentContainer = document.createElement('div');
	firstParentContainer.append(
		newParentInput(modules),
		newPlusButton(() => addParent(modules))
	);
	document.getElementById('parents').appendChild(firstParentContainer);
};

const addResource = (firstResource) => {

	// Type
	const typeLabelSpan = document.createElement('span');
	typeLabelSpan.innerHTML = 'Type';

	const typeDropdown = document.createElement('select');
	typeDropdown.className = 'resourceType';

	for (let type in resourceTypes) {
		const typeOption = document.createElement('option');
		typeOption.value = type;
		typeOption.innerHTML = resourceTypes[type];
		typeDropdown.appendChild(typeOption);
	}

	const typeLabel = document.createElement('label');
	typeLabel.append(
		typeLabelSpan,
		typeDropdown
	);

	// Format
	const formatLabelSpan = document.createElement('span');
	formatLabelSpan.innerHTML = 'Format';

	const formatDropdown = document.createElement('select');
	formatDropdown.className = 'resourceFormat';
	formatDropdown.appendChild(newDropdownOption('', 'None'));

	for (let format in formats) {
		const formatOption = document.createElement('option');
		formatOption.value = format;
		formatOption.innerHTML = formats[format];
		formatDropdown.appendChild(formatOption);
	}

	const formatLabel = document.createElement('label');
	formatLabel.append(
		formatLabelSpan,
		formatDropdown
	);

	// URL
	const urlLabelSpan = document.createElement('span');
	urlLabelSpan.innerHTML = 'URL';

	const urlField = document.createElement('input');
	urlField.className = 'resourceURL';
	urlField.type = 'url';
	urlField.placeholder = 'URL';

	const urlLabel = document.createElement('label');
	urlLabel.append(
		urlLabelSpan,
		urlField
	);

	// Resource fields div
	const resourceContainerDiv = document.createElement('div');
	resourceContainerDiv.append(
		typeLabel,
		formatLabel,
		urlLabel
	);

	// Resource div
	const resourceContainer = document.createElement('div');
	resourceContainer.className = 'resource';
	resourceContainer.appendChild(resourceContainerDiv);

	// Resource button
	if (firstResource) {
		// Add button
		const addResourceButton = document.createElement('button');
		addResourceButton.type = 'button';
		addResourceButton.innerHTML = '+';
		addResourceButton.addEventListener('click', () => addResource(false));
		resourceContainer.appendChild(addResourceButton);
	} else {
		// Removal button
		resourceContainer.id = 'resource' + resourceID;
		resourceContainer.appendChild(newRemoveButton('resource' + resourceID));
		resourceID += 1;
	}

	document.getElementById('resources').appendChild(resourceContainer);
};

const newModuleObject = () => {
	const moduleObject = {};
	moduleObject.name = document.getElementById('moduleName').value;
	moduleObject.url = document.getElementById('moduleURL').value;
	moduleObject.creators = [];
	moduleObject.parents = [];
	moduleObject.resources = [];

	// Read creator fields
	const creatorFields = document.getElementsByClassName('moduleCreator');
	for (let i = 0; i < creatorFields.length; i++) {
		moduleObject.creators.push(creatorFields.item(i).value);
	}

	// Read parent fields
	const parentFields = document.getElementsByClassName('moduleParent');
	for (let i = 0; i < parentFields.length; i++) {
		if (parentFields.item(i).value) {
			moduleObject.parents.push(parentFields.item(i).value);
		}
	}

	// Read resources
	const resources = document.getElementsByClassName('resource');
	for (let i = 0; i < resources.length; i++) {
		const resourceObject = {};
		resourceObject.type = resources.item(i).getElementsByClassName('resourceType')[0].value;

		const format = resources.item(i).getElementsByClassName('resourceFormat')[0].value;
		if (format) {
			resourceObject.format = format;
		}

		const url = resources.item(i).getElementsByClassName('resourceURL')[0].value;
		if (url) {
			resourceObject.url = url;
		}

		moduleObject.resources.push(resourceObject);
	}

	return moduleObject;
};

const submitModule = () => {
	backendRequest(
		'/module',
		(data) => {
			window.location.href = formatURL('/module', { id: data.id });
		},
		newModuleObject()
	);
};

let creatorID = 0;
let parentID = 0;
let resourceID = 0;

backendRequest('/list_creators', addFirstCreator);
backendRequest('/list_modules', addFirstParent);

addResource(true);

document.getElementById('previewButton').addEventListener('click', () => drawJson(newModuleObject()));
document.getElementById('submitButton').addEventListener('click', submitModule);
