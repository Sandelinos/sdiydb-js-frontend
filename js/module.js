import { formats, resourceTypes } from '/js/modules/constants.js';
import { backendRequest, drawJson, formatURL, urlParams } from '/js/modules/utils.js';

const drawModule = (module) => {
	drawJson(module);

	document.getElementById('moduleName').innerHTML = module.name;
	const moduleLink = document.getElementById('moduleLink');
	moduleLink.innerHTML = '🏠 ' + module.url;
	moduleLink.href = module.url;

	// Creators
	module.creators.forEach(creatorID => {
		backendRequest('/creator/' + creatorID, drawCreator);
	});

	// Parents
	if (module.parents) {
		const parentsContainerNode = document.createElement('div');
		parentsContainerNode.id = 'parentsContainer';

		const parentsNode = document.createElement('span');
		parentsNode.innerHTML = 'Based on: ';
		parentsContainerNode.appendChild(parentsNode);

		const parentsListNode = document.createElement('ul');
		parentsListNode.id = 'moduleParents';
		parentsContainerNode.appendChild(parentsListNode);


		document.getElementById('moduleInfo').insertBefore(parentsContainerNode, moduleLink);

		module.parents.forEach(parentID => {
			backendRequest('/module/' + parentID, drawParent);
		});
	}

	// Resources
	module.resources.forEach(resource => {
		const resourceNode = document.createElement('li');
		const linkNode = document.createElement('a');
		linkNode.innerHTML = resourceTypes[resource.type] + (resource.url ? '' : ' 🏠');
		linkNode.href = resource.url || module.url;
		resourceNode.appendChild(linkNode);
		if (resource.format) {
			const formatNode = document.createElement('p');
			formatNode.className = 'formatLabel';
			formatNode.innerHTML = formats[resource.format].toUpperCase();
			resourceNode.appendChild(formatNode);
		}
		document.getElementById('moduleResources').appendChild(resourceNode);
	});
};

const drawCreator = (creator) => {
	const creatorNode = document.createElement('a');
	creatorNode.innerHTML = creator.brand || creator.name;
	creatorNode.href = formatURL('/creator', { id: creator.id });
	document.getElementById('moduleCreators').appendChild(creatorNode);
};

const drawParent = (module) => {
	const parentNode = document.createElement('a');
	parentNode.innerHTML = module.name;
	parentNode.href = formatURL('/module', { id: module.id });
	document.getElementById('moduleParents').appendChild(parentNode);
};

backendRequest('/module/' + urlParams.get('id'), drawModule);
