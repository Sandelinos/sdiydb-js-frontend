import { backendRequest, formatURL } from '/js/modules/utils.js';

const drawModules = (modules) => {
	const moduleListNode = document.getElementById('modules');
	modules.forEach(module => {
		const moduleNode = document.createElement('li');
		const linkNode = document.createElement('a');
		linkNode.innerHTML = module.name;
		linkNode.href = formatURL('/module', { id: module.id });
		moduleNode.appendChild(linkNode);
		moduleListNode.appendChild(moduleNode);
	});
};

backendRequest('/list_modules', drawModules);
