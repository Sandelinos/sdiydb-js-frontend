import { backendRequest } from '/js/modules/utils.js';

const login = () => {
	const user = document.getElementById('userField').value;
	const password = document.getElementById('passwordField').value;

	backendRequest(
		'/login',
		() => alert('Login success!'),
		{
			user: user,
			password: password
		}
	);
};

document.getElementById('loginButton').addEventListener('click', login);
