Simple javascript + html frontend for The SynthDIY Database.

## Building

	make

To enable pretty urls, build with `CPPFLAGS=-Dpretty_urls`

	CPPFLAGS=-Dpretty_urls make
