OUTDIR = out

HTML_SOURCES := $(wildcard *.html.template)
HTML_FILES := $(addprefix $(OUTDIR)/, $(HTML_SOURCES:.template=))

JS_SOURCES := $(shell find js -name '*.js')
JS_FILES := $(addprefix $(OUTDIR)/, $(JS_SOURCES))

.PHONY: all
all: html js $(OUTDIR)/style.css

.PHONY: html
html: $(HTML_FILES)

.PHONY: js
js: $(JS_FILES)

$(OUTDIR)/%.html: %.html.template header.html | $(OUTDIR)
	cpp $(CPPFLAGS) -P $< $@

$(OUTDIR)/%.js: %.js | $(OUTDIR)
	@mkdir -p $(@D)
	cpp $(CPPFLAGS) -P $< $@

$(OUTDIR)/style.css: style.css | $(OUTDIR)
	cp $< $@

$(OUTDIR):
	mkdir $(OUTDIR)

clean:
	rm -r $(OUTDIR)/*
